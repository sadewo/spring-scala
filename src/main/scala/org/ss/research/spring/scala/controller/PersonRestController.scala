package org.ss.research.spring.scala.controller

import org.springframework.beans.factory.annotation.Autowired
import org.ss.research.spring.scala.service.TPersonService
import org.springframework.web.bind.annotation._
import java.util.logging.Logger
import org.ss.research.spring.scala.entity.Person
import scala.collection.JavaConversions
import java.util

/**
 * Created by SoftwareSeni on 4/4/2014.
 */
@RestController
class PersonRestController @Autowired()(personService: TPersonService) {

  val log: Logger = Logger.getLogger(this.getClass.getName)

  @RequestMapping(value = Array("/persons"), method = Array(RequestMethod.GET))
  def findAllPerson: util.List[Person] = {
    //new ResponseEntity[util.ArrayList[Person]](new util.ArrayList[Person](JavaConversions.seqAsJavaList(personService.findAll)), HttpStatus.OK)
    //personService.findAll
    JavaConversions.seqAsJavaList(personService.findAll)
  }

  @RequestMapping(value = Array("/person"), method = Array(RequestMethod.GET))
  def findPerson: Person = {
    val person = new Person()
    person.id = 1
    person.name = "kris"
    person
  }

}
