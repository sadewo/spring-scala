package org.ss.research.spring.scala.controller

import org.springframework.beans.factory.annotation.Autowired
import org.ss.research.spring.scala.service.TPersonService
import org.springframework.web.bind.annotation._
import java.util.logging.Logger
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import scala.collection.JavaConversions
import org.ss.research.spring.scala.entity.Person

/**
 * Created by SoftwareSeni on 4/4/2014.
 */
@Controller
class PersonController @Autowired()(personService: TPersonService) {

  val log: Logger = Logger.getLogger(this.getClass.getName)

  @RequestMapping(value = Array("/person/list"), method = Array(RequestMethod.GET))
  def findAllPerson(model: Model) = {
    model.addAttribute("persons", JavaConversions.seqAsJavaList(personService.findAll))
    "person_list"
  }

  @RequestMapping(value = Array("/person/"), method = Array(RequestMethod.GET))
  def findPerson: Person = {
    val person = new Person()
    person.id = 1
    person.name = "kris"
    person
  }

}
