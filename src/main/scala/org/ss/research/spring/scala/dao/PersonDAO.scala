package org.ss.research.spring.scala.dao

import org.ss.research.spring.scala.entity.{Address, Person}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.{RowMapper, JdbcTemplate}
import java.sql.ResultSet
import org.springframework.stereotype.Repository
import scala.collection.JavaConversions
import org.springframework.dao.EmptyResultDataAccessException

/**
 * Created by SoftwareSeni on 4/7/2014.
 */
@Repository
class PersonDAO @Autowired()(val jdbcTemplate: JdbcTemplate) extends TPersonDAO {

  def save(person: Person) = {
    val sql = "INSERT INTO person(name,age,address_id) VALUES(?,?,?) "

    jdbcTemplate.update(sql,
      person.name,
      new Integer(person.age),
      new Integer(person.address.id))
  }

  def findAll: List[Person] = {
    val sql = "SELECT *FROM person p " +
      "INNER JOIN address a ON a.id = p.address_id "

    JavaConversions.asScalaBuffer(jdbcTemplate.query(sql, new PersonRowMapper)).toList
  }

  def findById(id: Int): Person = {
    val sql = "SELECT *FROM person p " +
      "INNER JOIN address a ON a.id = p.address_id " +
      "WHERE p.id = ? "

    try {
      jdbcTemplate.queryForObject(sql, Array[Object](new Integer(id)), new PersonRowMapper)
    } catch {
      case ex: EmptyResultDataAccessException => null
    }
  }

  class PersonRowMapper extends RowMapper[Person] {

    def mapRow(rs: ResultSet, rowNum: Int): Person = {
      val address = new Address()
      address.id = rs.getInt("id")
      address.city = rs.getString("city")
      address.province = rs.getString("province")
      address.country = rs.getString("country")

      val person = new Person()
      person.id = rs.getInt("id")
      person.name = rs.getString("name")
      person.age = rs.getInt("age")
      person.address = address

      person
    }
  }

}
