package org.ss.research.spring.scala.service

import org.springframework.stereotype.Service

/**
 * Created by SoftwareSeni on 4/4/2014.
 */
@Service("nameService")
class NameService extends TNameService {

  def name = "World"
}
