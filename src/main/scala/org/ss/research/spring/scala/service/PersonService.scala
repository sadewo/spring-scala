package org.ss.research.spring.scala.service

import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import org.ss.research.spring.scala.dao.TPersonDAO
import org.ss.research.spring.scala.entity.Person

/**
 * Created by SoftwareSeni on 4/8/2014.
 */
@Service
class PersonService @Autowired()(personDAO: TPersonDAO) extends TPersonService {

  override def findAll: List[Person] = {
    personDAO.findAll
  }
}
