package org.ss.research.spring.scala.entity

/**
 * Created by SoftwareSeni on 4/7/2014.
 */
class Address(var id: Int, var city: String, var province: String, var country: String) {

  def this() {
    this(0, null, null, null)
  }
}
