package org.ss.research.spring.scala.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.beans.factory.annotation.Autowired
import org.ss.research.spring.scala.service.TNameService
import org.springframework.ui.Model
import java.util.logging.Logger

/**
 * Created by SoftwareSeni on 4/4/2014.
 */
@Controller
class HelloWorldController @Autowired()(nameService: TNameService) {

  val log: Logger = Logger.getLogger(this.getClass.getName)

  @RequestMapping(Array("/"))
  def index(model: Model) = {
    log.info("check")
    model.addAttribute("name", nameService.name)
    "index"
  }
}
