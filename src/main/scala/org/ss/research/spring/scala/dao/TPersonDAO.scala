package org.ss.research.spring.scala.dao

import org.ss.research.spring.scala.entity.Person

/**
 * Created by SoftwareSeni on 4/7/2014.
 */
trait TPersonDAO {

  def save (person: Person)

  def findAll: List[Person]

  def findById(id: Int): Person
}
