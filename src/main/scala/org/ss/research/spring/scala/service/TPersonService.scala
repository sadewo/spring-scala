package org.ss.research.spring.scala.service

import org.ss.research.spring.scala.entity.Person

/**
 * Created by SoftwareSeni on 4/8/2014.
 */
trait TPersonService {

  def findAll: List[Person]
}
