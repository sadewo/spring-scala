package org.ss.research.spring.scala.entity

import scala.beans.BeanProperty

/**
 * Created by SoftwareSeni on 4/7/2014.
 */
class Person(
              @BeanProperty var id: Int,
              @BeanProperty var name: String,
              @BeanProperty var age: Int,
              @BeanProperty var address: Address) extends Serializable {

  def this() {
    this(0, null, 0, null)
  }

  override def toString: String = s"id : $id, name : $name"
}
