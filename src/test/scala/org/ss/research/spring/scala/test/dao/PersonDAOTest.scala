package org.ss.research.spring.scala.test.dao

import org.springframework.test.context.{TestContextManager, ContextConfiguration}
import org.junit.{Assert, Test}
import org.ss.research.spring.scala.dao.{TPersonDAO}
import org.springframework.beans.factory.annotation.Autowired
import org.ss.research.spring.scala.entity.{Person, Address}
import org.springframework.transaction.annotation.Transactional

/**
 * Created by SoftwareSeni on 4/7/2014.
 */
@ContextConfiguration(Array("classpath:application-context.xml"))
class PersonDAOTest {

  @Autowired
  val personDA0: TPersonDAO = null
  new TestContextManager(this.getClass).prepareTestInstance(this)

  @Test
  def findAll = {
    for (person <- personDA0.findAll) {
      println(person)
    }
  }

  @Test
  def findById = {
    Assert.assertNotNull(personDA0.findById(1))
  }

  @Test
  @Transactional
  def save = {
    val address = new Address()
    address.id = -1

    val person = new Person()
    person.name = "kris"
    person.age = 27
    person.address = address

    personDA0.save(person)
  }

}
