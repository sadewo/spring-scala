package org.ss.research.spring.scala.test.controller

import org.springframework.test.context.{ContextConfiguration, TestContextManager}
import org.junit.{Before, Test}
import org.ss.research.spring.scala.service.TPersonService
import org.springframework.test.web.servlet.MockMvc
import org.mockito.{MockitoAnnotations, Mock, InjectMocks}
import org.ss.research.spring.scala.controller.PersonRestController
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter

/**
 * Created by SoftwareSeni on 4/7/2014.
 */
@ContextConfiguration(Array("classpath:application-context.xml"))
class PersonControllerTest {

  //var mockMvc: MockMvc
  //@InjectMocks
  //val personController: PersonController

  @Mock
  val personService: TPersonService = null
  new TestContextManager(this.getClass).prepareTestInstance(this)

  @Before
  def setup = {
   // MockitoAnnotations.initMocks(this)
    //mockMvc = standaloneSetup(controller).setMessageConverters(new MappingJackson2HttpMessageConverter()).build()
  }

  @Test
  def findAll = {
    for (person <- personService.findAll) {
      println(person)
    }
  }
}
