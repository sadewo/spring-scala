package org.ss.research.spring.scala.test.service

import org.springframework.test.context.{TestContextManager, ContextConfiguration}
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.ss.research.spring.scala.service.TPersonService

/**
 * Created by SoftwareSeni on 4/7/2014.
 */
@ContextConfiguration(Array("classpath:application-context.xml"))
class PersonServiceTest {

  @Autowired
  val personService: TPersonService = null
  new TestContextManager(this.getClass).prepareTestInstance(this)

  @Test
  def findAll = {
    for (person <- personService.findAll) {
      println(person)
    }
  }

}
